# /usr/bin/python3
#-*- coding: utf-8-*-
#
# 17-exemple-fork.py
# -------------------------------------
# Marc Gómez isx47797439
# @ edt ASIX M06 Curs 2018-2019
# Gener 2020
# -------------------------------------
import sys,os
print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())

pid=os.fork()
if pid != 0:
    #os.wait()
    print("Programa pare", os.getpid(), pid)
else:
    print("Programa fill", os.getpid(), pid)

print("deu!")
sys.exit(0)