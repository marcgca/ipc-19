#!/usr/bin/python3
#-*- coding: utf-8-*-
#
# 25-ps-client-one2one.py [-p port]
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------
import sys,socket,argparse
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description="""CAL server""")
parser.add_argument("server", type=str,\
        help="Servidor a connectar")
parser.add_argument("-p","--port",type=int,\
        default=50001, dest="port")
args=parser.parse_args()

HOST = args.server
PORT = args.port
#----------------------------

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
command = "ps ax"
pipeData = Popen(command,shell=True,stdout=PIPE)
for line in pipeData.stdout:
  s.send(line)
s.close()
sys.exit(0)
