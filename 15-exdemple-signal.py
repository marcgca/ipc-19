#!/usr/bin/python3
# coding: utf-8
# https://docs.python.org/3.6/library/subprocess.html#subprocess.Popen

# ctrl + c sigint -2
# ctrl + s sigstop -19
# ctrl + q sigcount -18
# sighub reload -1
# asociar un señal a una funcion

# https://docs.python.org/3.6/library/signal.html
# python3 prog.py 

import sys, os, signal

def myhandler(signum, frame):
    print("signal handler called with signal:", signum)
    print("hasta luego lucas")
    sys.exit(1)

def mydeath(signum, frame):
    print("signal handler called with signal:", signum)
    print("no hem dona la gana de morir")

signal.signal(signal.SIGALRM,myhandler) # 14
signal.signal(signal.SIGUSR2,myhandler) # 12
signal.signal(signal.SIGUSR1,mydeath) # 10
signal.signal(signal.SIGTERM,signal.SIG_IGN)
signal.signal(signal.SIGINT,signal.SIG_IGN)

# llma a sigalarm en 60 segundos
signal.alarm(60)

print(os.getpid())
while True:
    pass

signal.alarm(0)
sys.exit(0)
