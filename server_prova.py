import sys,socket, os, argparse, signal, time, select
from subprocess import Popen, PIPE

HOST = ''
PORT = 50001

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)
print(os.getpid())
conns=[s]
while True:
    actius,x,y = select.select(conns,[],[])
    for actual in actius:
        if actual == s:
            conn, addr = s.accept()
            conns.append(conn)
        else:
            command = actual.recv(1024)
            if not command:
                sys.stdout.write("Client finalitzat: %s \n" % (actual))
                actual.close()
                conns.remove(actual)
            else:
                pipeData = Popen(command, shell=True, stdout=PIPE, stderr=PIPE)
                for line in pipeData.stdout:
                    actual.sendall(line)
                for line in pipeData.stderr:
                    actual.sendall(line)
                actual.sendall(bytes(chr(4), 'utf-8'))
        
s.close()
sys.exit(0)