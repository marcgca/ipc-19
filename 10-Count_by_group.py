# /usr/bin/python
#-*- coding: utf-8-*-
#
# prog [-s gid | gname | numusers] users groups
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------
import sys, argparse
from functools import cmp_to_key

groupDict={}

parser = argparse.ArgumentParser(description=\
        """LListar els usuaris de file o stdin (format /etc/passwd)""")
parser.add_argument("-s", "--sort", type=str,\
        help="sort criteria: gid | gname | nusers", metavar="criteria",\
        choices=["gid", "gname", "nusers"], dest="criteria")
parser.add_argument("userFile", type=str,\
        help="user file (/etc/passwd style)", metavar="userFile")
parser.add_argument("groupFile", type=str,\
        help="group file (/etc/group style)", metavar="groupFile")
args=parser.parse_args()
#-------------------------------------------------
class UnixUser():
    """Classe UnixUser: prototipus de /etc/passwd
    login:passwd:uid:gid:gecos:home:shell"""
    def __init__(self,userLine):
        "Constructor objectes UnixUser"
        userField=userLine.split(":")
        self.login=userField[0]
        self.passwd=userField[1]
        self.uid=int(userField[2])
        self.gid=int(userField[3])
        self.gname=""
        if self.gid in groupDict:
            self.gname=groupDict[self.gid].gname
        self.gecos=userField[4]
        self.home=userField[5]
        self.shell=userField[6]
    def show(self):
        "Mostra les dades de l'usuari"
        print(f"login: {self.login} uid: {self.uid} gid: {self.gid} gecos: {self.gecos} home: {self.home} shell: {self.shell}")
    def __str__(self):
        "functió to_string"
        return "%s %s %d %d %s %s %s %s" %(self.login, self.passwd, self.uid, self.gid, self.gname, self.gecos, self.home, self.shell)
# -------------------------------------------------------

class UnixGroup():
  """Classe UnixGroup: prototipus de /etc/group
  gname_passwd:gid:listUsers"""
  def __init__(self,groupLine):
    "Constructor objectes UnixGroup"
    groupField = groupLine.split(":")
    self.gname = groupField[0]
    self.passwd = groupField[1]
    self.gid = int(groupField[2])
    self.userListStr = groupField[3]
    self.userList=[]
    if self.userListStr[:-1]:
      self.userList = self.userListStr[:-1].split(",")
  def __str__(self):
    "functió to_string d'un objecte UnixGroup"
    return "%s %d %s" % (self.gname, int(self.gid), self.userList)

#----------------------------------------
# Guardem els grups en una llista i en un diccionari pels usuaris
fileIn=open(args.groupFile,"r")
groupList=[]
for line in fileIn:
  oneGroup=UnixGroup(line)
  groupDict[oneGroup.gid] = oneGroup
  groupList.append(oneGroup)
fileIn.close()

#----------------------------------------

fileIn=open(args.userFile,"r")
userList=[]
for line in fileIn:
  oneUser=UnixUser(line)
  # Els users que siguin i no estiguin al grup se'ls afegirà
  if oneUser.login not in groupDict[oneUser.gid].userList:
    groupDict[oneUser.gid].userList.append(oneUser.login)
fileIn.close()

# Ordenem, amb list compression, segons el criteri especificat
if args.criteria=="gid":
    index = [ (key, groupDict[key].gid) for key in groupDict]
    index.sort(key=lambda x: x[1])
elif args.criteria=="gname":
    index = [ (key, groupDict[key].gname) for key in groupDict]
    index.sort(key=lambda x:  str.lower(x[1]))
else:
    index = [ (key, len(groupDict[key].userList), groupDict[key].gname) for key in groupDict ]
    index.sort(key=lambda x: (x[1], x[2]))

for key in index:
  print(groupDict[key[0]])
exit(0)