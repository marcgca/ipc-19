# /usr/bin/python3
#-*- coding: utf-8-*-
#
# exemple-echoClient.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
import sys,socket
HOST = ''
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST,PORT))
# Activem que escolti
s.listen(1)
# Que es quedi escoltant ad infinitum fins que s'hi connecten
conn, addr = s.accept()
print("Connected by", addr)
while True:
  data = conn.recv(1024) # Es queda penjat escoltant el que diu el client
  if not data: break # NO diu quan no hi han més dades
                     # Diu que s'ha tancat per l'altre extrem (client)
  conn.send(data)
conn.close()
sys.exit(0)


