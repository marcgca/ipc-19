# /usr/bin/python3
#-*- coding: utf-8-*-
#
# 23-daytime-server-one2one.py 
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
import sys,socket, os
from subprocess import Popen, PIPE
HOST = ''
PORT = 50001
command = ["date"]
print(os.getpid())
# Engegem el servidor
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))

# Escolta un per un als clients
while True:
    # Activem que escolti
    s.listen(1)
    # Que es quedi escoltant ad infinitum fins que s'hi connecten
    conn, addr = s.accept()
    print("Connected by", addr)
    pipeData = Popen(command, stdout=PIPE, shell=True)
    for line in pipeData.stdout:
        conn.send(line)
    # Tanquem la connexió
    conn.close()

sys.exit(0)


