#!/usr/bin/python3
#-*- coding: utf-8-*-
#
# Examen UF2 automatització de tasques
# server01.py [-d|--debug] [-p|--port]
#
# sigusr1: llista de peers i plega.
# sigusr2: count de listpeer i plega.
# sigterm: llista de peers, count i plega
# Implementar un servidor telnet. El servidor rep una instrucció enviada pel client
# i retorna el resultat de la instrucció associada.
# - processos executa "ps ax"
# - ports executa "netstat -puta"
# - whoareyou i qualsevol altre executa "uname -a"

# Si s’indica debug el server genera per stdout la traça de 
# cada connexió.
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Febrer 2020
# -------------------------------------
import sys,socket, os, argparse, signal, time
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description=\
        """Servidor que retorna el resultat d'una instrucció enviada pel client""")
parser.add_argument("-d", "--debug", help="debug level", \
        dest="debug", action='store_true')
parser.add_argument("-p", "--port", type=int,\
        help="port dessitjat (44444 per defecte)",\
        default=44444, dest="port")
args=parser.parse_args()

l_peers= []
HOST = ''
PORT = args.port
DEBUG = args.debug
#--------------------------------------------
# Funcions de les senyals
def llista_peers(signum, frame):
    global l_peers
    print(f"Llista peers: {l_peers}")
    sys.exit(1)

def count_peers(signum, frame):
    global l_peers
    conectats = len(l_peers)
    print(f"Número de conectats: {conectats}")
    sys.exit(2)

def llista_count(signum, frame):
    global l_peers
    conectats = len(l_peers)
    print(f"Llista peers: {l_peers} total: {conectats}")
    sys.exit(3)
#--------------------------------------------

pid=os.fork()
if pid !=0:
    print("Engegant el servei...", pid)
    sys.exit(0)

# Senyals
signal.signal(signal.SIGUSR1,llista_peers) # 10
signal.signal(signal.SIGUSR2,count_peers) # 12
signal.signal(signal.SIGTERM,llista_count) # 15
# Engegem el servidor
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
# Activem que escolti
s.listen(1)

# Escolta un per un als clients
while True:
    conn, addr = s.accept()
    print("Connected by", addr)
    l_peers.append(addr)

    while True:
        # Recollim la comanda del client
        command = conn.recv(1024)
        # Si no hi ha ordre, tanquem la connexió i seguim escoltant
        if not command: break
        # Si és processos
        if 'processos' in str(command, 'utf-8'):
            command = 'ps ax'
            pipeData = Popen(command,shell=True,stdout=PIPE,stderr=PIPE)
        # Si és ports
        elif 'ports' in str(command, 'utf-8'):
            command = 'netstat -puta'
            pipeData = Popen(command,shell=True,stdout=PIPE,stderr=PIPE)
        # Si és whoareyou o qualsevol altre
        else:
            command = 'uname -a'
            pipeData = Popen(command,shell=True,stdout=PIPE,stderr=PIPE)
        # Executem l'ordre amb stdout i stderr
        for line in pipeData.stdout:
            conn.send(line)
            if DEBUG:
                sys.stdout.write(str(line, 'utf-8'))
        for line in pipeData.stderr:
            conn.send(line)
            if DEBUG:
                sys.stderr.write(str(line, 'utf-8'))    

    conn.close()




