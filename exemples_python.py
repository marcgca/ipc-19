l1=[2,5,1,9,7]
l2=[("pere",18),("anna",23),("marta",18),("joan",15)]
l3=[("marta",18),("anna",23),("pere",18),("joan",15)]

>>> for nom,edat in l3:
...     print(nom,edat)
... 
marta 18
anna 23
pere 18
joan 15

d1={1:("root",1,1),2:("bin",2,2),1000:("pere",1000,100),1001:("anna",1001,100)}

for k,v in d1.items():
    print(k,v)

for tupla in sorted(d1,reverse=True):
    print(d1[tupla])

>>> for k in d1:
...     l_ord.append((d1[k][0],k))
... 

# list compression
>>> index=[ (d1[k][0],k) for k in d1]
>>> index
[('root', 1), ('bin', 2), ('pere', 1000), ('anna', 1001)]

>>> r1 =[ (n*2) for n in l1]
>>> r1
[2, 4, 10, 14, 18]

>>> r2=[ (n, n*2, n*n) for n in l1]
>>> r2
[(1, 2, 1), (2, 4, 4), (5, 10, 25), (7, 14, 49), (9, 18, 81)]

>>> r4=[(q,d,n) for n,d,q in r2]
>>> r4
[(1, 2, 1), (4, 4, 2), (25, 10, 5), (49, 14, 7), (81, 18, 9)]

>>> [ nom for nom,edat in l2]
['pere', 'anna', 'marta', 'joan']

>>> [ nom for nom,edat in l2]
['pere', 'anna', 'marta', 'joan']

>>> [ n for n in l1 if n>5 ]
[7, 9]

>>> [ n for n in l1 if n**3<100 ]
[1, 2]

>>> [ (k, d1[k][2]) for k in d1 if d1[k][0]>"m"]
[(1, 1), (1000, 100)]
