# /usr/bin/python3
#-*- coding: utf-8-*-
#
# 24-calendar-server-one2one.py [-p port] [-a any]
# sigusr1: llista de peers i plega.
# sigusr2: count de listpeer i plega.
# sigterm: llista de peers, count i plega
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------
import sys,socket, os, argparse, signal
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description=\
        """Server que retorna un calendari""")
parser.add_argument("-p", type=int,\
        help="port dessitjat (50001 per defecte)",\
        default=50001, dest="port")
parser.add_argument("-a", type=int,\
        help="any dessitjat (actual per defecte)",\
        default=2020, dest="year")
args=parsers.parse_args()

l_peers= []
HOST = ''
PORT = args.port
YEAR = args.year
#--------------------------------------------

def llista_peers(signum, frame):
    global l_peers
    print(f"Llista peers: {l_peers}")
    sys.exit(1)

def count_peers(signum, frame):
    global l_peers
    conectats = len(l_peers)
    print(f"Número de conectats: {conectats}")
    sys.exit(2)

def llista_count(signum, frame):
    global l_peers
    conectats = len(l_peers)
    print(f"Llista peers: {l_peers} total: {conectats}")
    sys.exit(3)


#--------------------------------------------

pid=os.fork()
if pid !=0:
        print("Engegant el servei...", pid)
        sys.exit(0)

command = ["cal -y "+YEAR]
# Senyals
signal.signal(signal.SIGUSR1,llista_peers) # 10
signal.signal(signal.SIGUSR2,count_peers) # 12
signal.signal(signal.SIGTERM,llista_count) # 15
# Engegem el servidor
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
# Activem que escolti
s.listen(1)

# Escolta un per un als clients
while True:
    # Que es quedi escoltant ad infinitum fins que s'hi connecten
    conn, addr = s.accept()
    l_peers.append(addr)
    print("Connected by", addr)
    pipeData = Popen(command, stdout=PIPE, shell=True)
    for line in pipeData.stdout:
        conn.send(line)
    # Tanquem la connexió
    conn.close()

sys.exit(0)


