# /usr/bin/python3
#-*- coding: utf-8-*-
#
# 22-daytime-client.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
import sys,socket
HOST = '18.130.88.101'
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.connect((HOST, PORT))
while True:
    data = s.recv(1024)
    if not data: break
    
    print('Received', repr(data))
s.close()
sys.exit(0)


