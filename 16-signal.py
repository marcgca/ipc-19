#!/usr/bin/python3
# coding: utf-8
# https://docs.python.org/3.6/library/subprocess.html#subprocess.Popen

# ctrl + c sigint -2
# ctrl + s sigstop -19
# ctrl + q sigcount -18
# sighub reload -1
# asociar un señal a una funcion
# kill -l mostra tots els senyals

# https://docs.python.org/3.6/library/signal.html
# python3 prog.py 

import sys, os, signal
segons_donats = int(sys.argv[1])
up = 0
down = 0

def myend(signum, frame):
    global up, down
    print(f"up: {up} down: {down}")
    sys.exit(1)

def myreload(signum, frame):
    global segons_donats
    signal.alarm(segons_donats)

def myup(signum, frame):
    global up
    up += 1
    segons_restants = signal.alarm(0)
    segons_restants += 60
    signal.alarm(segons_restants)

def mydown(signum, frame):
    global down
    down += 1
    segons_restants = signal.alarm(0)
    if segons_restants >= 60:
        segons_restants -= 60
    signal.alarm(segons_restants)

def mytemp(signum, frame):
    segons_restants = signal.alarm(0)
    print(f"segons restants: {segons_restants}")
    signal.alarm(segons_restants)

signal.signal(signal.SIGALRM,myend) # 14
signal.signal(signal.SIGUSR2,mydown) # 12
signal.signal(signal.SIGUSR1,myup) # 10
signal.signal(signal.SIGHUP,myreload) # 1
signal.signal(signal.SIGTERM,mytemp) # 15
signal.signal(signal.SIGINT,signal.SIG_IGN) # 2

signal.alarm(segons_donats)

print(os.getpid())
while True:
    pass

signal.alarm(0)
sys.exit(0)