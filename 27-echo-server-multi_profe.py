#!/usr/bin/python
#-*- coding: utf-8-*-
'''
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# ASIX Hisi2 M06-ASO UF2NF1-Scripts
# @edt Curs 2019-2020
# Gener 2020
# Echo server multiple-connexions
# -----------------------------------------------------------------
'''
import socket, sys, select, os

HOST = ''                 
PORT = 50002             
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, PORT))
s.listen(1)
print(os.getpid())
conns=[s]
while True:
    actius,x,y = select.select(conns,[],[])
    # actius és la llista de les connexions
    #  o socket que hi ha activitat
    for actual in actius:
        if actual == s: # si és una nova connexió
            conn, addr = s.accept()
            print('Connected by', addr)
            conns.append(conn)
        else: # és una connexió ja establerta
            data = actual.recv(1024)
            if not data:
                sys.stdout.write("Client finalitzat: %s \n" % (actual))
                actual.close()
                conns.remove(actual)
            else:
                actual.sendall(data)
                actual.sendall(bytes(chr(4), 'utf-8'),socket.MSG_DONTWAIT)
s.close()
sys.exit(0)


