# /usr/bin/python3
#-*- coding: utf-8-*-
#
# head [-n 5|10|15] file
#  10 lines per defecte, file
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2018
# -------------------------------------
# 

import sys, argparse
parser = argparse.ArgumentParser(description=\
        """Mostrar les N primeres línies """,\
        epilog="thats all folks")
parser.add_argument("-n","--nlin",type=int,\
        help="Número de línies",dest="nlin",\
        choices=[5,10,15],\
        metavar="numLines",default=10)
parser.add_argument("-f", "--fitxer",type=str,\
        help="fitxer a processar", metavar="file",\
        required=True)
args=parser.parse_args()
print(args)
MAXLIN=args.nlin
fileIn=open(args.fitxer,"r")
counter=0
for line in fileIn:
  counter+=1
  print(line, end=' ')
  if counter==MAXLIN: break
fileIn.close()
exit(0)