# /usr/bin/python3
#-*- coding: utf-8-*-
#
# 14-popen-sql-multi.py  -d database   [-c numclie]..
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
# Utilitza el docker edtasixm06/postgres
# -----------------------------------------
#commandLocal = "psql -qtA -F ';' lab_clinic -c 'select * from pacients;'"
#commandRemote = "psql -qtA -h i11 -U postgres -F ';' lab_clinic -c 'select * from pacients;'"
# -------------------------------------------
import sys
from subprocess import Popen, PIPE
import argparse
parser = argparse.ArgumentParser(description='Consulta SQL interactiva')
parser.add_argument("-d", "--database", type=str,\
        help="base de dades", metavar="database",\
        required=True)
parser.add_argument("-c", "--client", type=str,\
        help="id client", metavar="client",\
        required=True)
args = parser.parse_args()
 
cmd = f"psql -qtA -F',' -h 172.17.0.2 -U postgres  {args.database}"
pipeData = Popen(command, stdin=PIPE, stdout=PIPE, shell = True, bufsize=0, universal_newlines=True)


pipeData.stdin.write(args.sqlStatment+"\n\q\n")
 
for clie in args.client:
    pipeData.stdin.write(f"select * from clientes where num_clie = {clie};\n")
    print(pipeData.stdout.readline(), end="")

piepData.stdin.write(f"\q\n")
sys.exit(0)


