#!/usr/bin/python3
#-*- coding: utf-8-*-
#
# 26-telnet-server.py [-p port] [-d debug]
# sigusr1: llista de peers i plega.
# sigusr2: count de listpeer i plega.
# sigterm: llista de peers, count i plega
# Implementar un servidor i un client telnet. Client i server 
# fan un diàleg. Cal un senyal de “yatà” Usem chr(4).
# Si s’indica debug el server genera per stdout la traça de 
# cada connexió.
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------
import sys,socket, os, argparse, signal, time
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description=\
        """Server que guarda el contingut de ps del client""")
parser.add_argument("-p", type=int,\
        help="port dessitjat (50001 per defecte)",\
        default=50001, dest="port")
parser.add_argument("-d", help="debug level", \
        dest="debug", action='store_true')
args=parser.parse_args()

l_peers= []
HOST = ''
PORT = args.port
DEBUG = args.debug
#--------------------------------------------

def llista_peers(signum, frame):
    global l_peers
    print(f"Llista peers: {l_peers}")
    sys.exit(1)

def count_peers(signum, frame):
    global l_peers
    conectats = len(l_peers)
    print(f"Número de conectats: {conectats}")
    sys.exit(2)

def llista_count(signum, frame):
    global l_peers
    conectats = len(l_peers)
    print(f"Llista peers: {l_peers} total: {conectats}")
    sys.exit(3)
#--------------------------------------------

pid=os.fork()
if pid !=0:
    print("Engegant el servei...", pid)
    sys.exit(0)

# Senyals
signal.signal(signal.SIGUSR1,llista_peers) # 10
signal.signal(signal.SIGUSR2,count_peers) # 12
signal.signal(signal.SIGTERM,llista_count) # 15
# Engegem el servidor
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
# Activem que escolti
s.listen(1)

# Escolta un per un als clients
while True:
    conn, addr = s.accept()
    print("Connected by", addr)
    l_peers.append(addr)

    while True:
        # Recollim l'ordre del client
        command = conn.recv(1024)
        # Si no hi ha ordre, tanquem la connexió i seguim escoltant
        if not command: break
        # Executem l'ordre amb stdout i stderr
        pipeData = Popen(command,shell=True,stdout=PIPE,stderr=PIPE)
        for line in pipeData.stdout:
            conn.send(line)
            if DEBUG:
                sys.stdout.write(str(line, 'utf-8'))
        
        for line in pipeData.stderr:
            conn.send(line)
            # A revisar, si no hi és no ho envia (?)
            print("")
            if DEBUG:
                sys.stderr.write(str(line, 'utf-8'))
        # Enviem el "corto"
        conn.send(bytes(chr(4), 'utf-8'))    

    conn.close()




