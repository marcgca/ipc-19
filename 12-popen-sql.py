# /usr/bin/python3
#-*- coding: utf-8-*-
# https://docs.python.org/3/library/subprocess.html?highlight=popen#subprocess.Popen
#
# popen-sql.py
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------
from subprocess import Popen, PIPE

command = ["psql -qtA -F',' -h 172.17.0.2 -U edtasixm06 training -c 'select *from oficinas;'"]
# El popen és el CONSTRUCTOR que crea la pipe
pipeData = Popen(command, stdout=PIPE, shell=True)
#--------------------------
for line in pipeData.stdout:
    print(line.decode("utf-8"))

exit(0)