# !/usr/bin/python3.6
# -*-coding: utf-8-*-
#
# Escola del Treball Hisx1
# Autor: Marc G
# Hisx: isx47797439
# data: 09/01/2020
# Versió: 1

import sys
MAXLIN=10

fileIn=sys.stdin
if len(sys.argv)==2:
    fileIn=open(sys.argv[1],'r')

counter=0

for line in fileIn:
    counter+=1
    print(line, end=" ")
    if counter==MAXLIN:
        break

fileIn.close()
exit(0)