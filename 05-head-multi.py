# /usr/bin/python3
#-*- coding: utf-8-*-
#
# head [-n 5|10|15] [-f file]...
#  10 lines per defecte, file
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2018
# -------------------------------------
# 
import sys, argparse

fileList=[]
#parser = argparse.ArgumentParser(description=\
#        """Mostrar les N primeres línies """,\
#        epilog="thats all folks")
#parser.add_argument("-n","--nlin",type=int,\
#        help="Número de línies",dest="nlin",\
#        choices=[5,10,15],\
#        metavar="numLines",default=10)
#parser.add_argument("-f", "--fitxer",type=str,\
#        help="fitxer a processar", metavar="file",\
#        dest="fileList", action='append')

parser = argparse.ArgumentParser(description=\
        """Mostrar les N primeres línies """,\
        epilog="thats all folks")
parser.add_argument("-n","--nlin",type=int,\
        help="Número de línies",dest="nlin",\
        choices=[5,10,15],\
        metavar="numLines",default=10)
parser.add_argument("-f", "--fitxer",type=str,\
        help="fitxer a processar", metavar="file",\
        dest="fileList", action='append')
parser.add_argument("-v", "--verbose", action="store_true", default=False)
args=parser.parse_args()
print(args)

# --------------------------------------
MAXLIN=args.nlin


def headFile(fitxer):    
    fileIn=open(fitxer,"r")
    counter=0
    for line in fileIn:
        counter+=1
        print(line, end='')
        if counter==MAXLIN: break
    fileIn.close()

if args.fileList:
    for file in args.fileList:
        if args.verbose:
            print("---------")
        headFile(file)

exit(0)