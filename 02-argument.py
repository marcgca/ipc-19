#/usr/bin/python3

import argparse

parser = argparse.ArgumentParser(description="programa exemple de processar arguments", 
prog="02-arguments.py", epilog="hasta luegu")
parser.add_argument("-e", "--edat", help="edat a processar", type=int, dest="useredat")
parser.add_argument("-f", "--fit", type=str, help="fitxer a processar",
metavar="fitxer")
args=parser.parse_args()
print(parser)
print(args)
print(args.fitxer, args.useredat)