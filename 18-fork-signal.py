# /usr/bin/python3
#-*- coding: utf-8-*-
#
# 18-fork-signal.py
# Usant el programa d'exemple fork fer que el procés 
# fill (un while infinit) es governi amb senyals. Amb 
# siguser1 mostra "hola radiola" i amb sigusr2 mostra 
# "adeu andreu" i finalitza.
# -------------------------------------
# Marc Gómez isx47797439
# @ edt ASIX M06 Curs 2018-2019
# Gener 2020
# -------------------------------------
import sys,os, signal

def hola(signum, frame):
    print("hola radiola")

def adeu(signum, frame):
    print("adeu andreu")
    sys.exit(1)

#------------------------------
pid=os.fork()
if pid != 0:
    print("Programa fill", pid)
else:

    signal.signal(signal.SIGUSR1,hola) # 10
    signal.signal(signal.SIGUSR2,adeu) # 12
    while True:
        pass

sys.exit(0)