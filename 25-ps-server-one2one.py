#!/usr/bin/python3
#-*- coding: utf-8-*-
#
# 25-ps-server-one2one.py [-p port]
# Els clients es connecten a un servidor, envien un informe 
# consistent en fer "ps ax" i finalitzen la connexió. 
# El servidor rep l'informe del client i el desa a disc. 
# Cada informe es desa amb el format: ip-port-timestamt.log, 
# on timestamp té el format AADDMM-HHMMSS. Usar un servidor 
# com el de l'exercici anterior, un daemon governat per senyals.
# sigusr1: llista de peers i plega.
# sigusr2: count de listpeer i plega.
# sigterm: llista de peers, count i plega
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------
import sys,socket, os, argparse, signal, time
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description=\
        """Server que guarda el contingut de ps del client""")
parser.add_argument("-p", type=int,\
        help="port dessitjat (50001 per defecte)",\
        default=50003, dest="port")
args=parser.parse_args()

l_peers= []
HOST = ''
PORT = args.port
#--------------------------------------------

def llista_peers(signum, frame):
    global l_peers
    print(f"Llista peers: {l_peers}")
    sys.exit(1)

def count_peers(signum, frame):
    global l_peers
    conectats = len(l_peers)
    print(f"Número de conectats: {conectats}")
    sys.exit(2)

def llista_count(signum, frame):
    global l_peers
    conectats = len(l_peers)
    print(f"Llista peers: {l_peers} total: {conectats}")
    sys.exit(3)
#--------------------------------------------

pid=os.fork()
if pid !=0:
    print("Engegant el servei...", pid)
    sys.exit(0)

# Senyals
signal.signal(signal.SIGUSR1,llista_peers) # 10
signal.signal(signal.SIGUSR2,count_peers) # 12
signal.signal(signal.SIGTERM,llista_count) # 15
# Engegem el servidor
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
# Activem que escolti
s.listen(1)

# Escolta un per un als clients
while True:
    conn, addr = s.accept()
    print("Connected by", addr)
    l_peers.append(addr)
    ip = addr[0]
    port = addr[1]
    fileName="/tmp/%s-%s-%s.log" % (ip,port,time.strftime("%Y%m%d%H%M%s"))
    fileLog=open(fileName,"w")
    while True:
        data = conn.recv(1024)
        if not data: break
        fileLog.write(str(data))
    fileLog.close()
    conn.close()


