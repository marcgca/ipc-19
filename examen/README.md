# Examen Automatització de tasques

## Marc Gómez Cardona

## isx47797439 HISX2 06/02/2019

En aquesta prova crearem un servidor que rebrà, i atendrà, connexions una per una.

Direcció IP del servidor a Amazon: **3.8.16.68**

Port del servidor: **44444**

* Segons la comanda que doni el client rebrà un resultat diferent:
  
  * ***processos*** executa "*ps ax*" i retorna el resultat:
    
    ```bash
    [isx47797439@i08 examen]$ ncat 3.8.16.68 44444
    processos
      PID TTY      STAT   TIME COMMAND
        1 ?        Ss     0:02 /usr/lib/systemd/systemd --switched-root --system --deserialize 31
        2 ?        S      0:00 [kthreadd]
        3 ?        I<     0:00 [rcu_gp]
        4 ?        I<     0:00 [rcu_par_gp]
        6 ?        I<     0:00 [kworker/0:0H-kblockd]
        7 ?        I      0:00 [kworker/u30:0-flush-202:0]
    [...]
    ```
  
  * ***ports*** executa "*netsat -puta*" i retorna el resultat:
    
    ```bash
    ports
    Active Internet connections (servers and established)
    Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
    tcp        0      0 0.0.0.0:cognex-dataman  0.0.0.0:*               LISTEN      987/python3         
    tcp        0      0 0.0.0.0:ssh             0.0.0.0:*               LISTEN      -                   
    tcp        0      0 ip-172-3:cognex-dataman 129.red-2-136-113:52604 ESTABLISHED 987/python3 
    [...]        
    ```
  
  * ***whoareyou*** o qualsevol altre executa "*uname -a*" i retorna el resultat:
    
    ```bash
    whoareyou
    Linux ip-172-31-5-191.eu-west-2.compute.internal 5.0.13-200.fc29.x86_64 #1 SMP Mon May 6 00:49:54 UTC 2019 x86_64 x86_64 x86_64 GNU/Linux
    ```

* Per habilitar que la màquina d'Amazon acceptés connexions pel port indicat ho hem fet creant un grup a la secció *Security Groups*, anomenat *ports_examen_server*, obrint el port **44444** .

* Per a que el procés no es mori al sortir de la connexió ssh hem executat el servidor amb l'ordre *nohup*:
  
  ```bash
  [fedora@ip-172-31-5-191 examen_ipc]$ nohup python3 server01.py 
  ```


