# /usr/bin/python3
#-*- coding: utf-8-*-
#
# 19-exemple-execv.py
# Ídem anterior però ara el programa fill execula un 
# “ls -la /”. Executa un nou procés carregat amb execv. 
# Aprofitar per veure les fiferents variants de exec.
# -------------------------------------
# Marc Gómez isx47797439
# @ edt ASIX M06 Curs 2018-2019
# Gener 2020
# -------------------------------------
import sys,os, signal

print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())

pid=os.fork()
if pid != 0:
    print("Programa pare", os.getpid(), pid)  
    sys.exit(0)

print("Programa fill", os.getpid(), pid)

# execv
#os.execv("/usr/bin/ls", ["/usr/bin/ls", "-ls", "/"])
#os.execv("/usr/bin/bash",["/usr/bin/bash","/home/users/inf/hisx2/isx47797439/Documents/repos/ipc-19/show.sh"])

# execl
#os.execl("/usr/bin/ls", "/usr/bin/ls", "-ls", "/")
os.execle("/usr/bin/bash","/usr/bin/bash","/home/users/inf/hisx2/isx47797439/Documents/repos/ipc-19/show.sh",{"nom":"joan","edat":"25"})

# execlp
#os.execlp("ls", "ls", "-ls", "/")

# execve
#os.execve("/bin/bash", ["/bin/bash", "-ls", "/"], {"nom":"joan","edat":25})

print("adeu!")
sys.exit(0)