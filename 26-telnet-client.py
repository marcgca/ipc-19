#!/usr/bin/python3
#-*- coding: utf-8-*-
#
# 26-telnet-client.py -p port -s server
# sigusr1: llista de peers i plega.
# sigusr2: count de listpeer i plega.
# sigterm: llista de peers, count i plega
# Implementar un servidor i un client telnet. Client i server 
# fan un diàleg. Cal un senyal de “yatà” Usem chr(4).
# Si s’indica debug el server genera per stdout la traça de 
# cada connexió.
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------
import sys,socket,argparse
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description="""CAL server""")
parser.add_argument("-s", "--server", type=str,\
        help="Servidor a connectar", required=True, dest="server")
parser.add_argument("-p","--port",type=int,\
        default=50001, dest="port")
args=parser.parse_args()

HOST = args.server
PORT = args.port
#----------------------------

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.connect((HOST, PORT))

while True:
    # Registrem l'ordre i l'enviem
    command = input("telnet> ")
    # Si no hi ha ordre, sortim del bucle
    if not command: break
    s.send(bytes(command, 'utf-8'))
    # Recollim el resultat del server
    while True:
        data = s.recv(1024)
        # corto
        if bytes(chr(4), 'utf-8') in data: 
                print('Received', repr(data[:-1]))
                break
        print('Received', repr(data))
    
s.close()    
sys.exit(0)