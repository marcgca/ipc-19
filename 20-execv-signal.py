# /usr/bin/python3
#-*- coding: utf-8-*-
#
# 18-exemple-execv.py
# Usant l'exemple execv programar un procés pare que llança 
# un fill i finalitza. El procés fill executa amb execv el 
# programa python 16-signal.py al que li passa un 
# valor hardcoded de segons.
# -------------------------------------
# Marc Gómez isx47797439
# @ edt ASIX M06 Curs 2018-2019
# Gener 2020
# -------------------------------------
import sys,os, signal

print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())

pid=os.fork()
if pid != 0:
    print("Programa pare", os.getpid(), pid)  
    sys.exit(0)

print("Programa fill", os.getpid(), pid)

# execv
os.execv("/usr/bin/python3", ["/usr/bin/python3", "16-signal.py", "180"])

print("adeu!")
sys.exit(0)