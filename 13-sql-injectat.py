# /usr/bin/python3
#-*- coding: utf-8-*-
# https://docs.python.org/3/library/subprocess.html?highlight=popen#subprocess.Popen
#
# popen-sql.py
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------
import sys, argparse
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description="Exercici 13")
parser.add_argument("ruta", type=str,\
                    help="directori a llistar")
args=parser.parse_args()

#--------------------------------------

command = [f"psql -qtA -F',' -h 172.17.0.2 -U edtasixm06 training -c \"{args.ruta}\""]
# El popen és el CONSTRUCTOR que crea la pipe
pipeData = Popen(command, stdout=PIPE, shell=True)
#--------------------------
for line in pipeData.stdout:
    print(line.decode("utf-8"))

exit(0)